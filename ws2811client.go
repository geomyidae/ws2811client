// Copyright 2016 Jacques Supcik, Blue Masters
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ws2811client

import (
	log "github.com/sirupsen/logrus"
	"github.com/gorilla/http"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	urllib "net/url"
	"strconv"
	"time"
)

type Ws2811Client struct {
	conn       *websocket.Conn
	Url        *urllib.URL
	Brightness int
}

func LocalClient(url string) (*Ws2811Client, error) {
	// on localhost, url is usually ws://127.0.0.1:8484/
	u, err := urllib.Parse(url)
	if err != nil {
		return nil, err
	}
	return &Ws2811Client{
		Url: u,
	}, nil
}

func (c *Ws2811Client) Open(retry time.Duration) (code int, err error) {
	url := *c.Url
	url.Path = "/open"
	if c.Brightness > 0 {
		v := url.Query()
		v.Set("brightness", strconv.Itoa(c.Brightness))
		url.RawQuery = v.Encode()
	}

	log.Debugf("Dialing %v", url.String())
	dialer := websocket.Dialer{}
	for {
		conn, resp, err := dialer.Dial(url.String(), nil)
		if err == nil && resp.StatusCode == 101 {
			c.conn = conn
			return resp.StatusCode, err
		} else if retry == 0 {
			return resp.StatusCode, err
		}
		time.Sleep(retry)
	}
}

func (c *Ws2811Client) Close() {
	url := *c.Url
	url.Path = "/close"
	http.DefaultClient.Get(url.String(), nil)
}

func stripeToBytes(stripe []uint32) []byte {
	res := make([]byte, len(stripe)*3)
	for i, v := range stripe {
		res[i*3+0] = byte((v >> 16) % 256)
		res[i*3+1] = byte((v >> 8) % 256)
		res[i*3+2] = byte((v >> 0) % 256)
	}
	return res
}

func (c *Ws2811Client) Update(frame []uint32) error {
	if c.conn != nil {
		err := c.conn.WriteMessage(websocket.BinaryMessage, stripeToBytes(frame))
		if err == nil {
			_, _, err = c.conn.ReadMessage()
		}
		return err
	} else {
		return errors.Errorf("Connection closed")
	}
}
